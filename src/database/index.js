import tables from './createtables'
import methods from './methods'

let db
let indexedDB
let indexedDBAvailable = false

if (!window.indexedDB) {
  console.dir("Sorry, your browser doesn't support a stable version of IndexedDB")
} else {
  console.dir('Your browser supports a stable version of IndexedDB')
  indexedDBAvailable = true
}

if (indexedDBAvailable) {
  indexedDB = window.indexedDB.open('budgetAppDB', 1)
  indexedDB.onerror = async function(event) {
    console.dir('error: ' + event)
  }
  indexedDB.onsuccess = async function(event) {
    db = await indexedDB.result
    console.dir('EXISTING DB: ' + db)
  }
  indexedDB.onupgradeneeded = async function(event) {
    db = event.target.result
    console.dir('DB CREATED: ' + db)
    tables.createDB(db, 'users')
    tables.createDB(db, 'budget')
    tables.createDB(db, 'income')
    tables.createDB(db, 'expenses')
  }
}

export default {
  select(tableName, data) {
    return methods.select(data, tableName, db)
  },
  selectAll(tableName) {
    return methods.selectAll(tableName, db)
  },
  create(tableName, data) {
    return methods.create(data, tableName, db)
  },
  update(tableName, data) {
    return methods.update(data, tableName, db)
  },
  remove(tableName, data) {
    return methods.remove(data, tableName, db)
  }
}
