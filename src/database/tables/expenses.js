export default {
  async create(db) {
    const objectStore = await db.createObjectStore('expenses', {
      keyPath: 'id',
      autoIncrement: true
    })
    await objectStore.createIndex('id', 'id', { unique: true })
    await objectStore.createIndex('user_id', 'user_id', { unique: false })
    await objectStore.createIndex('name', 'name', { unique: false })
    await objectStore.createIndex('description', 'description', {
      unique: false
    })
    await objectStore.createIndex('amount', 'amount', { unique: false })
    await objectStore.createIndex('quantity', 'quantity', { unique: false })
    await objectStore.createIndex('category', 'category', { unique: false })
    await objectStore.createIndex('date', 'date', { unique: false })
  }
}
