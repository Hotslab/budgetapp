export default {
  async create(db) {
    const objectStore = await db.createObjectStore('users', {
      keyPath: 'email',
      autoIncrement: true
    })
    await objectStore.createIndex('id', 'id', { unique: true })
    await objectStore.createIndex('name', 'name', { unique: false })
    await objectStore.createIndex('surname', 'surname', { unique: true })
    await objectStore.createIndex('email', 'email', { unique: true })
    await objectStore.createIndex('currency', 'currency', { unique: false })
    await objectStore.createIndex('date', 'date', { unique: false })
    await objectStore.createIndex('password', 'password', { unique: false })
  }
}
