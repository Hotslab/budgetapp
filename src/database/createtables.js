import users from './tables/users'
import budget from './tables/budget'
import income from './tables/income'
import expenses from './tables/expenses'

export default {
  async createDB(db, name) {
    switch (name) {
      case 'users':
        await users.create(db)
        break
      case 'budget':
        await budget.create(db)
        break
      case 'income':
        await income.create(db)
        break
      case 'expenses':
        await expenses.create(db)
        break
      default:
        break
    }
  }
}
