function setResponse(objectStore, resolve, reject) {
  objectStore.onsuccess = (event) => {
    resolve({ success: true, data: event.target.result, error: null })
  }
  objectStore.onerror = (event) => {
    reject({ success: false, data: null, error: event.target.result })
  }
}

export default {
  async select(data, tableName, currentDB) {
    const objectStore = await currentDB.transaction([tableName], 'readonly')
      .objectStore(tableName)
      .get(data)
    return new Promise(function(resolve, reject) {
      setResponse(objectStore, resolve, reject)
    })
  },
  async selectAll(tableName, currentDB) {
    const objectStore = await currentDB.transaction([tableName], 'readonly')
      .objectStore(tableName)
      .getAll()
    return new Promise(function(resolve, reject) {
      setResponse(objectStore, resolve, reject)
    })
  },
  async create(data, tableName, currentDB) {
    const objectStore = await currentDB.transaction([tableName], 'readwrite')
      .objectStore(tableName)
      .add(data)
    return new Promise(function(resolve, reject) {
      setResponse(objectStore, resolve, reject)
    })
  },
  async update(data, tableName, currentDB) {
    const objectStore = await currentDB.transaction([tableName], 'readwrite')
      .objectStore(tableName)
      .put(data)
    return new Promise(function(resolve, reject) {
      setResponse(objectStore, resolve, reject)
    })
  },
  async remove(data, tableName, currentDB) {
    const objectStore = await currentDB.transaction([tableName], 'readwrite')
      .objectStore(tableName)
      .delete(data)
    return new Promise(function(resolve, reject) {
      setResponse(objectStore, resolve, reject)
    })
  }
}
