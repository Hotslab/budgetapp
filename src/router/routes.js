const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        name: 'login',
        component: () => import('pages/Login.vue')
      },
      {
        path: '/register',
        name: 'register',
        component: () => import('pages/Register.vue')
      },
      {
        path: '/dashboard',
        name: 'dashboard',
        component: () => import('pages/Dashboard.vue')
      },
      {
        path: '/income',
        name: 'income',
        component: () => import('pages/Income.vue')
      },
      {
        path: '/budget-expenses',
        name: 'budget-expenses',
        component: () => import('pages/BudgetExpenses.vue')
      },
      {
        path: '/expenses',
        name: 'expenses',
        component: () => import('pages/Expenses.vue')
      },
      {
        path: '/settings',
        name: 'settings',
        component: () => import('pages/Settings.vue')
      }
    ]
  }
]

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}

export default routes
