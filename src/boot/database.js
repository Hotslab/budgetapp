import DB from '../database'

const database = {
  install(Vue) {
    Vue.prototype.$indexedDB = DB
  }
}

export default ({ Vue }) => {
  Vue.use(database)
}
